local M = {}

local harpoon = require("harpoon")

function M.setup()
  harpoon:setup()
end

-- basic telescope configuration
function M.toggle_telescope()
  local harpoon_files = harpoon:list()
  local conf = require("telescope.config").values

  local file_paths = {}
  for _, item in ipairs(harpoon_files.items) do
    table.insert(file_paths, item.value)
  end

  require("telescope.pickers")
    .new({}, {
      prompt_title = "Harpoon",
      finder = require("telescope.finders").new_table({
        results = file_paths,
      }),
      previewer = conf.file_previewer({}),
      sorter = conf.generic_sorter({}),
    })
    :find()
end

function M.append()
  harpoon:list():add()
end

function M.toggle_quick_menu()
  harpoon.ui:toggle_quick_menu(harpoon:list())
end

function M.select(i)
  harpoon:list():select(i)
end

function M.removeCurrent()
  harpoon:list():remove()
end

function M.remove(i)
  harpoon:list():remove_at(i)
end

function M.prev()
  harpoon:list():prev()
end

function M.next()
  harpoon:list():next()
end

return M
