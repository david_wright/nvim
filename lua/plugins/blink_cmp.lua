local opts = {

  signature = { enabled = true },

  snippets = { preset = "luasnip" },

  sources = {
    default = { "lsp", "path", "snippets", "buffer", "copilot", "markdown" },
    providers = {
      copilot = {
        name = "copilot",
        module = "blink-cmp-copilot",
        score_offset = 100,
        async = true,
        transform_items = function(_, items)
          local CompletionItemKind = require("blink.cmp.types").CompletionItemKind
          local kind_idx = #CompletionItemKind + 1
          CompletionItemKind[kind_idx] = "Copilot"
          for _, item in ipairs(items) do
            item.kind = kind_idx
          end
          return items
        end,
      },
      markdown = {
        name = "RenderMarkdown",
        module = "render-markdown.integ.blink",
        fallbacks = { "lsp" },
      },
    },
  },

  completion = {
    menu = {
      draw = {
        columns = { { "item_idx" }, { "kind_icon" }, { "label", "label_description", gap = 1 } },
        components = {
          item_idx = {
            text = function(ctx)
              return tostring(ctx.idx)
            end,
            highlight = "BlinkCmpItemIdx", -- optional, only if you want to change its color
          },
        },
      },
    },
  },

  keymap = {
    preset = "default",
    ["<Tab>"] = {
      function(cmp)
        if cmp.snippet_active() then
          return cmp.accept()
        else
          return cmp.select_and_accept()
        end
      end,
      "snippet_forward",
      "fallback",
    },
    ["<S-Tab>"] = { "snippet_backward", "fallback" },
    ["<Up>"] = { "select_prev", "fallback" },
    ["<Down>"] = { "select_next", "fallback" },
    ["<A-1>"] = {
      function(cmp)
        cmp.accept({ index = 1 })
      end,
    },
    ["<A-2>"] = {
      function(cmp)
        cmp.accept({ index = 2 })
      end,
    },
    ["<A-3>"] = {
      function(cmp)
        cmp.accept({ index = 3 })
      end,
    },
    ["<A-4>"] = {
      function(cmp)
        cmp.accept({ index = 4 })
      end,
    },
    ["<A-5>"] = {
      function(cmp)
        cmp.accept({ index = 5 })
      end,
    },
    ["<A-6>"] = {
      function(cmp)
        cmp.accept({ index = 6 })
      end,
    },
    ["<A-7>"] = {
      function(cmp)
        cmp.accept({ index = 7 })
      end,
    },
    ["<A-8>"] = {
      function(cmp)
        cmp.accept({ index = 8 })
      end,
    },
    ["<A-9>"] = {
      function(cmp)
        cmp.accept({ index = 9 })
      end,
    },
  },

  appearance = {
    nerd_font_variant = "mono",
    kind_icons = {
      Copilot = "",
      Text = "󰉿",
      Method = "󰊕",
      Function = "󰊕",
      Constructor = "󰒓",

      Field = "󰜢",
      Variable = "󰆦",
      Property = "󰖷",

      Class = "󱡠",
      Interface = "󱡠",
      Struct = "󱡠",
      Module = "󰅩",

      Unit = "󰪚",
      Value = "󰦨",
      Enum = "󰦨",
      EnumMember = "󰦨",

      Keyword = "󰻾",
      Constant = "󰏿",

      Snippet = "󱄽",
      Color = "󰏘",
      File = "󰈔",
      Reference = "󰬲",
      Folder = "󰉋",
      Event = "󱐋",
      Operator = "󰪚",
      TypeParameter = "󰬛",
    },
  },
}

return opts
