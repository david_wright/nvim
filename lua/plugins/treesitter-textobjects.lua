local M = {}

function M.setup()
  require("nvim-treesitter.configs").setup({
    textobjects = {
      select = {
        enable = true,
        lookahead = true, -- Automatically jump forward to textobj
        keymaps = {
          -- Function text objects
          ["af"] = "@function.outer",
          ["if"] = "@function.inner",

          -- Class text objects
          ["ac"] = "@class.outer",
          ["ic"] = "@class.inner",

          -- Conditional (if, switch, etc.) text objects
          ["ai"] = "@conditional.outer",
          ["ii"] = "@conditional.inner",

          -- Loop text objects (for, while, etc.)
          ["al"] = "@loop.outer",
          ["il"] = "@loop.inner",

          -- Parameter text objects (function arguments)
          ["ap"] = "@parameter.outer",
          ["ip"] = "@parameter.inner",

          -- Block text objects (code blocks or scopes)
          ["ab"] = "@block.outer",
          ["ib"] = "@block.inner",

          -- Return statement text objects
          ["ar"] = "@return.outer",
          ["ir"] = "@return.inner",

          -- Comment text objects
          ["ak"] = "@comment.outer",
          ["ik"] = "@comment.inner",
        },
      },
      move = {
        enable = true,
        set_jumps = true, -- Whether to set jumps in the jumplist
        goto_next_start = {
          ["]a"] = "@assignment.outer",
          ["]f"] = "@function.outer",
          ["]c"] = "@class.outer",
          ["]k"] = "@comment.outer",
          ["]i"] = "@conditional.outer",
          ["]l"] = "@loop.outer",
          ["]b"] = "@block.outer",
          ["]p"] = "@parameter.outer",
        },
        goto_next_end = {
          ["]A"] = "@assignment.outer",
          ["]F"] = "@function.outer",
          ["]C"] = "@class.outer",
          ["]K"] = "@comment.outer",
          ["]I"] = "@conditional.outer",
          ["]L"] = "@loop.outer",
          ["]B"] = "@block.outer",
          ["]P"] = "@parameter.outer",
        },
        goto_previous_start = {
          ["[a"] = "@assignment.outer",
          ["[f"] = "@function.outer",
          ["[c"] = "@class.outer",
          ["[k"] = "@comment.outer",
          ["[i"] = "@conditional.outer",
          ["[l"] = "@loop.outer",
          ["[b"] = "@block.outer",
          ["[p"] = "@parameter.outer",
        },
        goto_previous_end = {
          ["[A"] = "@assignment.outer",
          ["[F"] = "@function.outer",
          ["[C"] = "@class.outer",
          ["[K"] = "@comment.outer",
          ["[I"] = "@conditional.outer",
          ["[L"] = "@loop.outer",
          ["[B"] = "@block.outer",
          ["[P"] = "@parameter.outer",
        },
      },
      swap = {
        enable = true,
        swap_next = {
          ["<leader>a"] = "@assignment.outer",
          ["<leader>f"] = "@function.outer",
          ["<leader>c"] = "@class.outer",
          ["<leader>i"] = "@conditional.outer",
          ["<leader>l"] = "@loop.outer",
          ["<leader>b"] = "@block.outer",
          ["<leader>p"] = "@parameter.outer",
        },
        swap_previous = {
          ["<leader>A"] = "@assignment.outer",
          ["<leader>F"] = "@function.outer",
          ["<leader>C"] = "@class.outer",
          ["<leader>I"] = "@conditional.outer",
          ["<leader>L"] = "@loop.outer",
          ["<leader>B"] = "@block.outer",
          ["<leader>P"] = "@parameter.outer",
        },
      },
    },
  })
end

return M
