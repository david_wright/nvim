local M = {}

function M.setup()
  require("gitlab").setup({
    port = nil,
    log_path = vim.fn.stdpath("cache") .. "/gitlab.nvim.log",
    reviewer = "diffview",
    attachment_dir = nil,
    discussion_sign_and_diagnostic = {
      skip_resolved_discussion = false,
      skip_old_revision_discussion = true,
    },
    discussion_signs = {
      enabled = true, -- Show diagnostics for gitlab comments in the reviewer
      skip_resolved_discussion = false, -- Show diagnostics for resolved discussions
      severity = vim.diagnostic.severity.INFO, -- ERROR, WARN, INFO, or HINT
      virtual_text = true, -- Whether to show the comment text inline as floating virtual text
      priority = 100, -- Higher will override LSP warnings, etc
      icons = {
        comment = "💬",
        range = " |",
      },
    },
    discussion_diagnostic = {
      -- If you want to customize diagnostics for discussions you can make special config
      -- for namespace `gitlab_discussion`. See :h vim.diagnostic.config
      enabled = true,
      severity = vim.diagnostic.severity.INFO,
      code = nil, -- see :h diagnostic-structure
      display_opts = {}, -- see opts in vim.diagnostic.set
    },
    pipeline = {
      created = "",
      pending = "",
      preparing = "",
      scheduled = "",
      running = "",
      canceled = "↪",
      skipped = "↪",
      success = "✓",
      failed = "",
    },
  })
end

return M
