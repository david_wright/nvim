local M = {}

function M.setup()
  require("neorg").setup({
    load = {
      ["core.defaults"] = {},
      ["core.concealer"] = {},
      ["core.dirman"] = {
        config = {
          workspaces = {
            notes = "~/notes",
          },
        },
      },
      ["core.integrations.telescope"] = {},
    },
  })

  local neorg_callbacks = require("neorg.core.callbacks")

  neorg_callbacks.on_event("core.keybinds.events.enable_keybinds", function(_, keybinds)
    keybinds.map_event_to_mode("norg", {
      n = {
        { "<C-s>", "core.integrations.telescope.find_linkable" },
      },

      i = {
        { "<C-l>", "core.integrations.telescope.insert_link" },
      },
    }, {
      silent = true,
      noremap = true,
    })
  end)
end

return M
