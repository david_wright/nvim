local M = {}

function M.setup()
  require("neogen").setup({ snippet_engine = "luasnip" })
end

return M
