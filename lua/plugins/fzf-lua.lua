local M = {}

function M.setup()
  require("fzf-lua").setup({
    previewers = {
      builtin = {
        extensions = {
          ["bmp"] = { "chafa" },
          ["png"] = { "chafa" },
          ["svg"] = { "chafa" },
          ["jpg"] = { "chafa" },
        },
      },
    },
  })
end

return M
