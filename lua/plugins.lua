-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

return require("lazy").setup({
  pkg = {
    enabled = true,
    cache = vim.fn.stdpath("state") .. "/lazy/pkg-cache.lua",
    versions = true, -- Honor versions in pkg sources
    -- the first package source that is found for a plugin will be used.
    sources = {
      "lazy",
      "rockspec",
      "packspec",
    },
  },
  rocks = {
    root = vim.fn.stdpath("data") .. "/lazy-rocks",
    server = "https://nvim-neorocks.github.io/rocks-binaries/",
  },
  spec = {
    -- better UI
    {
      "stevearc/dressing.nvim",
      config = function()
        require("plugins/dressing").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- better UI for messages, cmdline and the popupmenu
    {
      "folke/noice.nvim",
      event = "VeryLazy",
      opts = {
        -- add any options here
      },
      dependencies = {
        "MunifTanjim/nui.nvim",
        "rcarriga/nvim-notify",
      },
      config = function()
        require("plugins/noice").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- Mason package manager for lsp servers, dap, etc.
    {
      "williamboman/mason-lspconfig.nvim",
      config = function()
        require("mason-lspconfig").setup({
          ensure_installed = {
            "lua_ls",
            "rust_analyzer",
            "cmake",
            "basedpyright",
            "dockerls",
            "jsonls",
            "clangd",
            "jdtls",
            "buf_ls",
            "marksman",
          },
          automatic_installation = true,
        })
      end,
      cond = not vim.g.vscode,
    },

    {
      "williamboman/mason.nvim",
      config = function()
        require("mason").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- Local config files
    {
      "klen/nvim-config-local",
      config = function()
        require("config-local").setup({
          -- Default configuration (optional)
          config_files = { ".vimrc.lua", ".vimrc" }, -- Config file patterns to load (lua supported)
          hashfile = vim.fn.stdpath("data") .. "/config-local", -- Where the plugin keeps files data
          autocommands_create = true, -- Create autocommands (VimEnter, DirectoryChanged)
          commands_create = true, -- Create commands (ConfigSource, ConfigEdit, ConfigTrust, ConfigIgnore)
          silent = false, -- Disable plugin messages (Config loaded/ignored)
          lookup_parents = false, -- Lookup config files in parent directories
        })
      end,
    },
    -- Keybindings configuration / visualisation
    -- Note: Keybindings are configured in keybindings.lua for better self-documentation
    {
      "folke/which-key.nvim",
    },

    -- File Explorer
    {
      "nvim-neo-tree/neo-tree.nvim",
      branch = "v2.x",
      dependencies = {
        "nvim-lua/plenary.nvim",
        "nvim-tree/nvim-web-devicons",
        "MunifTanjim/nui.nvim",
        {
          "s1n7ax/nvim-window-picker",
          version = "v1.*",
          config = function()
            require("window-picker").setup({
              autoselect_one = true,
              include_current = false,
              filter_rules = {
                bo = {
                  filetype = { "neo-tree", "neo-tree-popup", "notify" },
                  buftype = { "terminal", "quickfix" },
                },
              },
              other_win_hl_color = "#e35e4f",
            })
          end,
        },
      },
      config = function()
        require("plugins/neotree").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- -- Preview vim register contents
    -- { "tversteeg/registers.nvim" },

    -- bufferline ("tabs")
    {
      "akinsho/bufferline.nvim",
      dependencies = { "nvim-tree/nvim-web-devicons", "catppuccin/nvim" },
      config = function()
        require("plugins/bufferline").setup()
      end,
    },

    -- Formatters
    {
      "stevearc/conform.nvim",
      config = function()
        require("conform").setup({
          formatters_by_ft = {
            lua = { "stylua" },
            python = { "isort", "black" },
            javascript = { { "prettierd", "prettier" } },
            typescript = { { "prettierd", "prettier" } },
            cpp = { "clang-format" },
            c = { "clang-format" },
            cmake = { "cmake-format" },
            sh = { "shfmt" },
            just = { "just" },
            markdown = { "prettier" },
            yaml = { "prettier" },
            rust = { "rustfmt" },
            html = { "prettier" },
            xml = { "xmlformat" },
            haskell = { "ormolu" },
          },
          formatters = {
            rustfmt = { command = "cargo fmt" },
          },
          -- format_on_save = {
          --   -- These options will be passed to conform.format()
          --   timeout_ms = 500,
          --   lsp_fallback = true,
          -- },
        })
      end,
      cond = not vim.g.vscode,
    },

    -- Telescope, for fuzzy finders/browsers
    {
      "nvim-telescope/telescope.nvim",
      dependencies = { "nvim-lua/plenary.nvim", "BurntSushi/ripgrep", "nvim-telescope/telescope-media-files.nvim" },
      config = function()
        require("plugins/telescope").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- FZF, for fuzzy finders/browsers
    {
      "ibhagwan/fzf-lua",
      requires = { "nvim-tree/nvim-web-devicons" },
      config = function()
        require("plugins/fzf-lua").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- Code completion
    {
      "saghen/blink.cmp",
      dependencies = {
        "rafamadriz/friendly-snippets",
        { "L3MON4D3/LuaSnip", version = "v2.*" },
        "giuxtaposition/blink-cmp-copilot",
        "zbirenbaum/copilot.lua",
        "MeanderingProgrammer/render-markdown.nvim",
      },
      version = "*",
      -- build = "cargo build --release",
      opts = require("plugins/blink_cmp"),
      opts_extend = { "sources.default" },
      cond = not vim.g.vscode,
    },

    -- Code snippets
    {
      "L3mon4d3/LuaSnip",
      config = function()
        require("plugins/luasnip").setup()
      end,
    },

    -- Source code documentation generator
    {
      "danymat/neogen",
      dependencies = "nvim-treesitter/nvim-treesitter",
      config = function()
        require("plugins/neogen").setup()
      end,
    },

    -- Syntax-aware text objects (e.g. for functions, arguments, etc.)
    {
      "nvim-treesitter/nvim-treesitter-textobjects",
      dependencies = "nvim-treesitter/nvim-treesitter",
      config = function()
        require("plugins/treesitter-textobjects").setup()
      end,
    },

    -- Configs for the built-in Language Server Protocol
    {
      "neovim/nvim-lspconfig",
      dependencies = {
        "mfussenegger/nvim-jdtls",
        "williamboman/mason-lspconfig.nvim",
        "williamboman/mason.nvim",
        "saghen/blink.cmp",
      },
      config = function()
        require("plugins/lspconfig").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- Lsp additions
    {
      "glepnir/lspsaga.nvim",
      branch = "main",
      dependencies = { "catppuccin/nvim", "lewis6991/gitsigns.nvim" },
      config = function()
        require("plugins/lspsaga").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- LSP diagnostics on the line below the diagnostic occurred rather than at the end of it
    -- {
    --   "https://git.sr.ht/~whynothugo/lsp_lines.nvim",
    --   config = function()
    --     -- Disabled for now as I'm not sure I like the additional clutter of the inserted lines
    --     -- require("plugins/lsp_lines").setup()
    --   end,
    -- },

    -- clangd extensions (such as inlay hints)
    {
      "p00f/clangd_extensions.nvim",
      dependencies = "neovim/nvim-lspconfig",
      config = function()
        require("plugins/clangd").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- Displaying errors/warnings in a window
    {
      "folke/trouble.nvim",
      dependencies = "nvim-tree/nvim-web-devicons",
      config = function()
        require("trouble").setup({})
      end,
      cond = not vim.g.vscode,
    },

    -- cmake
    {
      "Civitasv/cmake-tools.nvim",
      dependencies = { "nvim-lua/plenary.nvim", "stevearc/overseer.nvim", "akinsho/toggleterm.nvim" },
      config = function()
        require("plugins/cmake").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- Inlay hints for various language servers
    {
      "lvimuser/lsp-inlayhints.nvim",
      config = function()
        require("lsp-inlayhints").setup({
          inlay_hints = {
            parameter_hints = {
              show = true,
              prefix = "<- ",
              separator = ", ",
              remove_colon_start = false,
              remove_colon_end = true,
            },
            type_hints = {
              show = true,
              prefix = "",
              separator = ", ",
              remove_colon_start = false,
              remove_colon_end = true,
            },
            only_current_line = false,
            labels_separator = "  ",
            max_len_align = false,
            max_len_align_padding = 1,
            highlight = "LspInlayHint",
            priority = 0,
          },
          enabled_at_startup = true,
          debug_mode = false,
        })
      end,
      cond = not vim.g.vscode,
    },

    -- rust
    {
      "mrcjkb/rustaceanvim",
      dependencies = { "neovim/nvim-lspconfig", "lvimuser/lsp-inlayhints.nvim" },
      config = function()
        require("plugins/rust_tools").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- rust crates
    {
      "saecki/crates.nvim",
      dependencies = "mrcjkb/rustaceanvim",
      requires = { "nvim-lua/plenary.nvim" },
      config = function()
        require("crates").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- typescript
    {
      "pmizio/typescript-tools.nvim",
      dependencies = { "nvim-lua/plenary.nvim", "neovim/nvim-lspconfig" },
      opts = {},
      cond = not vim.g.vscode,
    },

    -- haskell
    {
      "mrcjkb/haskell-tools.nvim",
      lazy = false, -- This plugin is already lazy
    },

    -- csv
    { "mechatroner/rainbow_csv" },

    -- Rainbow delimiters (braces, brackets, etc.)
    {
      "hiphish/rainbow-delimiters.nvim",
      config = function()
        require("plugins/rainbow_delimiters").setup()
      end,
    },

    -- -- comments
    -- {
    --   "numToStr/Comment.nvim",
    --   config = function()
    --     require("Comment").setup()
    --   end,
    -- },

    -- git

    {
      "NeogitOrg/neogit",
      dependencies = { "nvim-lua/plenary.nvim", "sindrets/diffview.nvim", "nvim-telescope/telescope.nvim" },
      version = "v2.0.0",
      config = function()
        require("plugins/neogit").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- git blame
    {
      "FabijanZulj/blame.nvim",
      config = function()
        require("blame").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- diffing/merging
    {
      "sindrets/diffview.nvim",
      dependencies = "nvim-lua/plenary.nvim",
      config = function()
        require("plugins/diffview").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- debugging
    {
      "rcarriga/nvim-dap-ui",
      dependencies = {
        "mfussenegger/nvim-dap",
        "mfussenegger/nvim-dap-python",
        "theHamsta/nvim-dap-virtual-text",
        "jbyuki/one-small-step-for-vimkind",
      },
      config = function()
        require("plugins/debugging").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- Mason configuration for dap
    {
      "jayp0521/mason-nvim-dap.nvim",
      config = function()
        require("mason-nvim-dap").setup({
          automatic_installation = true,
          ensure_installed = { "python", "cppdbg", "codelldb" },
        })
      end,
      cond = not vim.g.vscode,
    },

    -- Mason configuration for other tools
    {
      "WhoIsSethDaniel/mason-tool-installer.nvim",
      config = function()
        require("mason-tool-installer").setup({
          ensure_installed = {
            "stylua",
            "black",
            "luacheck",
            "shfmt",
            "pylint",
            "sonarlint-language-server",
            "clang-format",
            "markdownlint",
            "prettier",
            "yamlfmt",
            "cmakelang",
            "haskell-language-server",
            "haskell-debug-adapter",
          },
          auto_update = false,
          run_on_start = true,
          start_delay = 0,
          debounce_hours = 5,
        })
      end,
      cond = not vim.g.vscode,
    },

    -- Testing
    {
      "nvim-neotest/neotest",
      dependencies = {
        "nvim-lua/plenary.nvim",
        "nvim-treesitter/nvim-treesitter",
        "antoinemadec/FixCursorHold.nvim",
        "alfaix/neotest-gtest",
        "nvim-neotest/neotest-python",
        "mrcjkb/rustaceanvim",
        "andy-bell101/neotest-java",
        "nvim-neotest/nvim-nio",
      },
      config = function()
        require("plugins/neotest").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- Tresitter for minimal syntax highlighting
    {
      "nvim-treesitter/nvim-treesitter",
      build = ":TSUpdate",
      config = function()
        require("plugins/treesitter").setup()
      end,
    },

    -- Syntax highlighting for log files
    {
      "fei6409/log-highlight.nvim",
      config = function()
        require("log-highlight").setup({})
      end,
    },

    -- startup.nvim startup manager
    {
      "startup-nvim/startup.nvim",
      dependencies = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" },
      config = function()
        require("startup").setup({ theme = "dashboard" })
      end,
      cond = not vim.g.vscode,
    },

    -- Statusline
    {
      "nvim-lualine/lualine.nvim",
      dependencies = {
        "nvim-tree/nvim-web-devicons",
        "mortepau/codicons.nvim",
        "hrsh7th/nvim-cmp", -- I actually don't want to use this, how to disable?
        "https://codeberg.org/esensar/nvim-dev-container",
      },
      config = function()
        require("plugins/lualine").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- undotree visualiser
    {
      "mbbill/undotree",
      config = function()
        require("plugins/undotree").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- Highlight & search todos
    {
      "folke/todo-comments.nvim",
      dependencies = "nvim-lua/plenary.nvim",
      config = function()
        require("plugins/todo-comments").setup()
      end,
    },

    -- UI based search/replace
    {
      "VonHeikemen/searchbox.nvim",
      dependencies = {
        { "MunifTanjim/nui.nvim" },
      },
      config = function()
        require("plugins/searchbox").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- Colour theme
    {
      "catppuccin/nvim",
      config = function()
        require("plugins/colourscheme").setup()
      end,
    },

    -- Indentation guides
    {
      "lukas-reineke/indent-blankline.nvim",
      config = function()
        require("plugins/indent_blankline").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- Automatic indentation
    {
      "nmac427/guess-indent.nvim",
      config = function()
        require("guess-indent").setup({})
      end,
    },

    -- jump motions
    {
      "ggandor/leap.nvim",
      dependencies = "tpope/vim-repeat",
      config = function()
        require("plugins/leap").setup()
      end,
    },

    -- folds
    {
      "kevinhwang91/nvim-ufo",
      dependencies = { "kevinhwang91/promise-async", "nvim-treesitter/nvim-treesitter" },
      config = function()
        require("plugins/nvim_ufo").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- statuscol
    {
      "luukvbaal/statuscol.nvim",
      dependencies = { "mfussenegger/nvim-dap", "lewis6991/gitsigns.nvim" },
      config = function()
        require("plugins/statuscol").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- camel case or snake case motion
    {
      "chaoren/vim-wordmotion",
      config = function() end,
    },

    -- Highlight git changes in statuscol
    {
      "lewis6991/gitsigns.nvim",
      dependencies = { "petertriho/nvim-scrollbar" },
      config = function()
        require("plugins/gitsigns").setup()
      end,
      cond = not vim.g.vscode,
    },

    -- Show current code context
    {
      "SmiteshP/nvim-navic",
      requires = "neovim/nvim-lspconfig",
      config = function()
        require("plugins/navic").setup()
      end,
    },

    -- Statusline built on navic to show the current code context
    {
      "utilyre/barbecue.nvim",
      dependencies = {
        "SmiteshP/nvim-navic",
        "nvim-tree/nvim-web-devicons", -- optional dependency
      },
      config = function()
        require("plugins/barbecue").setup()
      end,
    },

    -- Autopairs
    {
      "echasnovski/mini.pairs",
      version = false,
      config = function()
        require("plugins/mini-pairs").setup()
      end,
    },

    -- Jump forward/backward to various target types (e.g conflict marker, comment block)
    {
      "echasnovski/mini.bracketed",
      version = false,
      config = function()
        require("plugins/mini-bracketed").setup()
      end,
    },

    -- Moving text
    {
      "echasnovski/mini.nvim",
      version = false,
      config = function()
        require("plugins/mini-move").setup()
      end,
    },

    -- Search & replace
    {
      "windwp/nvim-spectre",
      dependencies = { "nvim-lua/plenary.nvim", "BurntSushi/ripgrep" },
      config = function()
        require("plugins/spectre").setup()
      end,
      cond = not vim.g.vscode,
    },

    { "kevinhwang91/promise-async", cond = not vim.g.vscode },
    { "MunifTanjim/nui.nvim", cond = not vim.g.vscode },
    -- {
    --   "edluffy/hologram.nvim",
    --   config = function()
    --     require("plugins/hologram").setup()
    --   end,
    -- },

    -- Show hex colours
    {
      "NvChad/nvim-colorizer.lua",
      config = function()
        require("colorizer").setup()
      end,
    },

    {
      "Zeioth/markmap.nvim",
      build = "yarn global add markmap-cli",
      cmd = { "MarkmapOpen", "MarkmapSave", "MarkmapWatch", "MarkmapWatchStop" },
      opts = {
        html_output = "",
        hide_toolbar = false,
        grace_period = 0,
      },
      config = function(_, opts)
        require("markmap").setup(opts)
      end,
      cond = not vim.g.vscode,
    },

    {
      "zbirenbaum/copilot.lua",
      cmd = "Copilot",
      event = "InsertEnter",
      config = function()
        require("plugins/copilot").setup()
      end,
      cond = not vim.g.vscode,
    },

    {
      "harrisoncramer/gitlab.nvim",
      dependencies = {
        "MunifTanjim/nui.nvim",
        "nvim-lua/plenary.nvim",
        "stevearc/dressing.nvim",
        enabled = true,
      },
      build = function()
        require("gitlab.server").build(true)
      end,
      config = function()
        require("plugins/gitlab").setup()
      end,
      cond = not vim.g.vscode,
    },

    {
      "simrat39/symbols-outline.nvim",
      config = function()
        require("plugins/symbols-outline").setup()
      end,
      cond = not vim.g.vscode,
    },

    {
      "nvim-neorg/neorg",
      dependencies = {
        "nvim-lua/plenary.nvim",
        "nvim-neorg/neorg-telescope",
        "nvim-neorg/lua-utils.nvim",
        "pysan3/pathlib.nvim",
      },
      config = function()
        require("plugins/neorg").setup()
      end,
      cond = not vim.g.vscode,
    },

    {
      "3rd/image.nvim",
      config = function()
        require("plugins/image").setup()
      end,
      cond = not vim.g.vscode,
    },

    {
      "kevinhwang91/nvim-hlslens",
      dependencies = { "petertriho/nvim-scrollbar" },
      config = function()
        require("scrollbar.handlers.search").setup({})
      end,
      cond = not vim.g.vscode,
    },

    {
      "petertriho/nvim-scrollbar",
      config = function()
        require("plugins/scrollbar").setup()
      end,
      cond = not vim.g.vscode,
    },

    {
      "MeanderingProgrammer/render-markdown.nvim",
      config = function()
        require("plugins/render_markdown").setup()
      end,
      dependencies = { "nvim-treesitter/nvim-treesitter", "nvim-tree/nvim-web-devicons" },
      cond = not vim.g.vscode,
    },

    -- LSP for languages embedded into documents of other languages (e.g. C++ in a markdown document)
    {
      "jmbuhr/otter.nvim",
      dependencies = {
        "nvim-treesitter/nvim-treesitter",
      },
      config = function()
        require("plugins/otter").setup()
      end,
      cond = not vim.g.vscode,
    },

    {
      "johmsalas/text-case.nvim",
      config = function()
        require("textcase").setup({})
      end,
      keys = {
        "ga",
      },
      lazy = false,
    },

    -- Mark files and positions in files to navigate between
    {
      "ThePrimeagen/harpoon",
      branch = "harpoon2",
      dependencies = { "nvim-lua/plenary.nvim", "nvim-telescope/telescope.nvim" },
      config = function()
        require("plugins/harpoon").setup()
      end,
    },

    -- Emojis
    {
      "allaman/emoji.nvim",
      dependencies = {
        "nvim-telescope/telescope.nvim",
      },
      config = function()
        require("plugins/emoji").setup()
      end,
      cond = not vim.g.vscode,
    },

    {
      "kawre/leetcode.nvim",
      build = ":TSUpdate html",
      dependencies = {
        "nvim-telescope/telescope.nvim",
        "nvim-lua/plenary.nvim",
        "MunifTanjim/nui.nvim",
        "nvim-treesitter/nvim-treesitter",
        "rcarriga/nvim-notify",
        "nvim-tree/nvim-web-devicons",
        "3rd/image.nvim",
      },
      opts = {
        plugins = { non_standalone = true },
        image_support = true,
      },
      cond = not vim.g.vscode,
    },
    {
      "amitds1997/remote-nvim.nvim",
      version = "*",
      dependencies = {
        "nvim-lua/plenary.nvim",
        "MunifTanjim/nui.nvim",
        "nvim-telescope/telescope.nvim",
      },
      config = function()
        require("plugins/remote").setup()
      end,
      cond = not vim.g.vscode,
    },

    { "https://github.com/leafo/magick", cond = not vim.g.vscode },
    -- sonarlint
    -- {
    --   "https://gitlab.com/schrieveslaach/sonarlint.nvim",
    --   config = function()
    --     require("sonarlint").setup({
    --       server = {
    --         cmd = {
    --           "sonarlint-language-server",
    --           "-stdio",
    --           "-analyzers",
    --           vim.fn.expand("$MASON/share/sonarlint-analyzers/sonarpython.jar"),
    --           vim.fn.expand("$MASON/share/sonarlint-analyzers/sonarjava.jar"),
    --           vim.fn.expand("$MASON/share/sonarlint-analyzers/sonarcfamily.jar"),
    --         },
    --       },
    --       filetypes = {
    --         "python",
    --         -- "java",
    --         "cpp",
    --         "c",
    --       },
    --     })
    --   end,
    -- },
  },
})
