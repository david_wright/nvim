local M = {}

function M.setup()
  require("which-key").setup({})
  require("which-key").add({
    -- Folds group
    { "z", group = "Folds" },
    { "zo", "<cmd>foldopen<cr>", desc = "Open fold", cond = not vim.g.vscode },
    { "zo", "<cmd>lua require('vscode').action('editor.unfold')<cr>", desc = "Open fold", cond = vim.g.vscode ~= nil},
    { "zc", "<cmd>foldclose<cr>", desc = "Close fold", cond = not vim.g.vscode },
    { "zc", "<cmd>lua require('vscode').action('editor.fold')<cr>", desc = "Close fold", cond = vim.g.vscode ~= nil },

    -- Goto group
    { "g", group = "Goto" },
    { "gb", "<cmd>bnext<cr>", desc = "Next Buffer" },
    { "gB", "<cmd>bprev<cr>", desc = "Previous Buffer" },
    { "gd", "<cmd>Trouble lsp_definitions<cr>", desc = "Go to definition", cond = not vim.g.vscode },
    { "gD", "<cmd>Lspsaga peek_definition<cr>", desc = "Peek definition", cond = not vim.g.vscode },

    -- Diagnostics navigation
    {
      "[d",
      "<cmd>lua require('lspsaga.diagnostic'):goto_prev()<cr>",
      desc = "Jump to previous diagnostic",
      cond = not vim.g.vscode,
    },
    {
      "[d",
      "<cmd>lua require('vscode').action('editor.action.marker.prev')<cr>",
      desc = "Jump to previous diagnostic",
      cond = vim.g.vscode ~= nil,
    },
    {
      "[e",
      "<cmd>lua require('lspsaga.diagnostic'):goto_prev({ severity = vim.diagnostic.severity.ERROR })<cr>",
      desc = "Jump to previous error",
      cond = not vim.g.vscode,
    },
    {
      "]d",
      "<cmd>lua require('lspsaga.diagnostic'):goto_next()<cr>",
      desc = "Jump to next diagnostic",
      cond = not vim.g.vscode,
    },
    {
      "]d",
      "<cmd>lua require('vscode').action('editor.action.marker.next')<cr>",
      desc = "Jump to next diagnostic",
      cond = vim.g.vscode ~= nil,
    },
    {
      "]e",
      "<cmd>lua require('lspsaga.diagnostic'):goto_next({ severity = vim.diagnostic.severity.ERROR })<cr>",
      desc = "Jump to next error",
      cond = not vim.g.vscode,
    },
    -- Leader mappings
    { "<leader>", group = "Leader" },

    -- Annotations group
    { "<leader>a", group = "Annotations" },
    { "<leader>ag", "<cmd>lua require('neogen').generate()<cr>", desc = "Generate Documentation" },

    -- Buffer group
    { "<leader>b", group = "Buffer" },
    {
      "<leader>bf",
      "<cmd>lua require'conform'.format({['bufnr'] = vim.api.nvim_get_current_buf(), ['lsp_fallback'] = true, ['timeout'] = 500})<cr><cmd>w<cr>",
      desc = "Format",
      cond = not vim.g.vscode,
    },
    { "<leader>bh", "<C-w>h", desc = "Go to the window to the left" },
    { "<leader>bl", "<C-w>l", desc = "Go to the window to the right" },
    { "<leader>bj", "<C-w>j", desc = "Go to the window below" },
    { "<leader>bk", "<C-w>k", desc = "Go to the window above" },
    { "<leader>b<", "15<C-w><", desc = "Decrease buffer width" },
    { "<leader>b>", "15<C-w>>", desc = "Increase buffer width" },
    { "<leader>b-", "15<C-w>-", desc = "Decrease buffer height" },
    { "<leader>b+", "15<C-w>+", desc = "Increase buffer height" },

    -- CMake group
    { "<leader>c", group = "CMake", cond = not vim.g.vscode },
    { "<leader>cc", "<cmd>CMakeSelectConfigurePreset<cr>", desc = "Select configure preset", cond = not vim.g.vscode },
    { "<leader>cg", "<cmd>CMakeGenerate<cr>", desc = "Generate", cond = not vim.g.vscode },
    { "<leader>ct", "<cmd>CMakeSelectBuildTarget<cr>", desc = "Select build target", cond = not vim.g.vscode },
    { "<leader>cb", "<cmd>wa<cr><cmd>CMakeBuild<cr>", desc = "Build", cond = not vim.g.vscode },
    { "<leader>cT", "<cmd>CMakeSelectLaunchTarget<cr>", desc = "Select launch target", cond = not vim.g.vscode },
    { "<leader>cr", "<cmd>wa<cr><cmd>CMakeRun<cr>", desc = "Run", cond = not vim.g.vscode },
    { "<leader>cd", "<cmd>wa<cr><cmd>CMakeDebug<cr>", desc = "Debug", cond = not vim.g.vscode },
    { "<leader>cs", "<cmd>CMakeStopExecutor<cr>", desc = "Stop Executor", cond = not vim.g.vscode },
    { "<leader>cS", "<cmd>CMakeStopRunner<cr>", desc = "Stop Runner", cond = not vim.g.vscode },

    -- Debug group
    { "<leader>d", group = "Debug", cond = not vim.g.vscode },
    {
      "<leader>db",
      "<cmd>lua require'dap'.toggle_breakpoint()<cr>",
      desc = "Toggle breakpoint",
      cond = not vim.g.vscode,
    },
    {
      "<leader>de",
      "<cmd>lua require'dapui'.eval()<cr><cmd>lua require'dapui'.eval()<cr>",
      desc = "Evaluate expression under cursor",
      cond = not vim.g.vscode,
    },
    {
      "<leader>dl",
      "<cmd>CMakeCloseExecutor<cr><cmd>CMakeCloseRunner<cr><cmd>lua require'dap.ext.vscode'.load_launchjs(nil, { cpp = {'c', 'cpp', 'python'} })<cr><cmd>lua require'dap'.continue()<cr>",
      desc = "Launch debug session",
      cond = not vim.g.vscode,
    },
    {
      "<leader>dt",
      "<cmd>lua require'dap'.terminate()<cr><cmd>lua require'dapui'.close()<cr>",
      desc = "Terminate debug session",
      cond = not vim.g.vscode,
    },

    -- Fuzzy finder group
    { "<leader>f", group = "Fuzzy Finder" },
    { "<leader>ff", "<cmd>Telescope find_files hidden=true<cr>", desc = "Find files", cond = not vim.g.vscode },
    { "<leader>ff", "<cmd>lua require('vscode').action('omni-search-ui:quick-search-filename')<cr>", desc = "Find Files", cond = vim.g.vscode ~= nil },
    { "<leader>fg", "<cmd>Telescope live_grep<cr>", desc = "Live grep", cond = not vim.g.vscode },
    { "<leader>fg", "<cmd>lua require('vscode').action('omni-search-ui:quick-search-biggrep')<cr>", desc = "BigGrep", cond = vim.g.vscode ~= nil },
    { "<leader>fb", "<cmd>Telescope buffers<cr>", desc = "Buffers" },
    { "<leader>fe", "<cmd>Telescope emoji<cr>", desc = "Emoji" },
    { "<leader>fh", "<cmd>lua require('plugins.harpoon').toggle_telescope()<cr>", desc = "Harpoon" },
    { "<leader>fm", "<cmd>Telescope media_files<cr>", desc = "Media files" },
    { "<leader>fo", "<cmd>Telescope oldfiles<cr>", desc = "Recent files" },

    -- git group
    { "<leader>g", group = "Git" },
    { "<leader>go", "<cmd>Neogit<cr>", desc = "Open Neogit" },
    { "<leader>gb", "<cmd>BlameToggle<cr>", desc = "Open git blame" },

    -- GitLab group
    { "<leader>gl", group = "GitLab" },
    { "<leader>glo", "<cmd>lua require('gitlab').review()<cr>", desc = "Open Merge Request Review" },
    { "<leader>glO", "<cmd>lua require('gitlab').create_mr()<cr>", desc = "Create Merge Request" },
    { "<leader>glA", "<cmd>lua require('gitlab').approve()<cr>", desc = "Approve Merge Request" },
    { "<leader>glR", "<cmd>lua require('gitlab').revoke()<cr>", desc = "Revoke Merge Request Approval" },
    { "<leader>glc", "<cmd>lua require('gitlab').create_comment()<cr>", desc = "Create Comment" },
    { "<leader>gln", "<cmd>lua require('gitlab').create_note()<cr>", desc = "Create Note" },
    {
      "<leader>gld",
      "<cmd>lua require('gitlab').move_to_discussion_tree_from_diagnostic()<cr>",
      desc = "Go to discussion",
    },
    { "<leader>glp", "<cmd>lua require('gitlab').pipeline()<cr>", desc = "Open Pipeline" },
    { "<leader>glb", "<cmd>lua require('gitlab').open_in_browser()<cr>", desc = "Open in Browser" },

    -- GitLab Assignees group
    { "<leader>gla", group = "Assignees" },
    { "<leader>glaa", "<cmd>lua require('gitlab').add_assignee()<cr>", desc = "Add Assignee" },
    { "<leader>glad", "<cmd>lua require('gitlab').delete_assignee()<cr>", desc = "Delete Assignee" },

    -- GitLab Reviewers group
    { "<leader>glr", group = "Reviewers" },
    { "<leader>glra", "<cmd>lua require('gitlab').add_reviewer()<cr>", desc = "Add Reviewer" },
    { "<leader>glrd", "<cmd>lua require('gitlab').delete_reviewer()<cr>", desc = "Delete Reviewer" },

    { "<leader>gls", "<cmd>lua require('gitlab').create_comment_suggestion()<cr>", desc = "Create change suggestion" },
    { "<leader>glS", "<cmd>lua require('gitlab').summary()<cr>", desc = "Merge Request Summary" },

    { "<leader>h", group = "Harpoon" },
    { "<leader>ha", "<cmd>lua require('plugins/harpoon').append()<cr>", desc = "Add file" },
    { "<leader>he", "<cmd>lua require('plugins/harpoon').toggle_quick_menu()<cr>", desc = "Toggle quick menu" },
    { "<leader>hp", "<cmd>lua require('plugins/harpoon').prev()<cr>", desc = "Select previous mark" },
    { "<leader>hn", "<cmd>lua require('plugins/harpoon').next()<cr>", desc = "Select next mark" },
    { "<leader>h1", "<cmd>lua require('plugins/harpoon').select(1)<cr>", desc = "Select mark 1" },
    { "<leader>h2", "<cmd>lua require('plugins/harpoon').select(2)<cr>", desc = "Select mark 2" },
    { "<leader>h3", "<cmd>lua require('plugins/harpoon').select(3)<cr>", desc = "Select mark 3" },
    { "<leader>h4", "<cmd>lua require('plugins/harpoon').select(4)<cr>", desc = "Select mark 4" },
    { "<leader>h5", "<cmd>lua require('plugins/harpoon').select(5)<cr>", desc = "Select mark 5" },
    { "<leader>h6", "<cmd>lua require('plugins/harpoon').select(6)<cr>", desc = "Select mark 6" },
    { "<leader>h7", "<cmd>lua require('plugins/harpoon').select(7)<cr>", desc = "Select mark 7" },
    { "<leader>h8", "<cmd>lua require('plugins/harpoon').select(8)<cr>", desc = "Select mark 8" },
    { "<leader>h9", "<cmd>lua require('plugins/harpoon').select(9)<cr>", desc = "Select mark 9" },

    -- Harpoon Remove group
    { "<leader>hr", group = "Remove" },
    {
      "<leader>hrc",
      "<cmd>lua require('plugins/harpoon').removeCurrent()<cr>",
      desc = "Remove current buffer from Harpoon list",
    },
    { "<leader>hr1", "<cmd>lua require('plugins/harpoon').remove(1)<cr>", desc = "Remove mark 1" },
    { "<leader>hr2", "<cmd>lua require('plugins/harpoon').remove(2)<cr>", desc = "Remove mark 2" },
    { "<leader>hr3", "<cmd>lua require('plugins/harpoon').remove(3)<cr>", desc = "Remove mark 3" },
    { "<leader>hr4", "<cmd>lua require('plugins/harpoon').remove(4)<cr>", desc = "Remove mark 4" },
    { "<leader>hr5", "<cmd>lua require('plugins/harpoon').remove(5)<cr>", desc = "Remove mark 5" },
    { "<leader>hr6", "<cmd>lua require('plugins/harpoon').remove(6)<cr>", desc = "Remove mark 6" },
    { "<leader>hr7", "<cmd>lua require('plugins/harpoon').remove(7)<cr>", desc = "Remove mark 7" },
    { "<leader>hr8", "<cmd>lua require('plugins/harpoon').remove(8)<cr>", desc = "Remove mark 8" },
    { "<leader>hr9", "<cmd>lua require('plugins/harpoon').remove(9)<cr>", desc = "Remove mark 9" },

    -- LSP group
    { "<leader>l", group = "LSP" },
    { "<leader>la", "<cmd>Lspsaga code_action<cr>", desc = "Code actions", cond = not vim.g.vscode },
    { "<leader>lh", "<cmd>ClangdSwitchSourceHeader<cr>", desc = "Toggle header/source", cond = not vim.g.vscode },
    {
      "<leader>lh",
      "<cmd>lua require('vscode').action('cpp:switch-header-source')<cr>",
      desc = "Toggle header/source",
      cond = vim.g.vscode ~= nil,
    },
    { "<leader>lo", "<cmd>SymbolsOutline<cr>", desc = "Toggle symbols outline", cond = not vim.g.vscode },
    { "<leader>lu", "<cmd>Trouble lsp_references<cr>", desc = "Show usages", cond = not vim.g.vscode },
    {
      "<leader>lu",
      "<cmd>lua require('vscode').action('references-view.findReferences')<cr>",
      desc = "Show usages",
      cond = vim.g.vscode ~= nil,
    },
    { "<leader>lr", "<cmd>Lspsaga rename<cr>", desc = "Rename", cond = not vim.g.vscode },
    { "<leader>lx", "<cmd>Lspsaga show_line_diagnostics<cr>", desc = "Show line diagnostics", cond = not vim.g.vscode },

    -- Leetcode group
    { "<leader>L", group = "Leetcode" },
    {
      "<leader>Lo",
      "<cmd>Leet<cr><cmd>LspStop<cr><cmd>lua require('cmp').setup { enabled = false }<cr>",
      desc = "Open",
    },

    -- Markdown group
    { "<leader>m", group = "Markdown" },
    { "<leader>mt", "<cmd>MarkdownPreviewToggle<cr>", desc = "Toggle preview" },

    -- Rust group
    { "<leader>r", group = "Rust" },
    { "<leader>rh", "<cmd>lua vim.cmd.RustLsp { 'hover', 'actions' }<cr>", desc = "Hover actions" },
    { "<leader>ra", "<cmd>lua vim.cmd.RustLsp('codeAction')<cr>", desc = "Code actions" },
    { "<leader>rr", "<cmd>wa<cr><cmd>lua vim.cmd.RustLsp {'runnables'}<cr>", desc = "Runnables" },
    { "<leader>rd", "<cmd>wa<cr><cmd>lua vim.cmd.RustLsp {'debuggables'}<cr>", desc = "Debuggables" },
    { "<leader>re", "<cmd>lua vim.cmd.RustLsp {'explainError' }<cr>", desc = "Explain Error" },

    -- Rust Crates group
    { "<leader>rc", group = "Crates" },
    { "<leader>rct", "<cmd>lua require('crates').toggle()<cr>", desc = "Toggle crates" },
    { "<leader>rcr", "<cmd>lua require('crates').reload()<cr>", desc = "Reload crates" },
    { "<leader>rcv", "<cmd>lua require('crates').show_versions_popup()<cr>", desc = "Show versions popup" },
    { "<leader>rcf", "<cmd>lua require('crates').show_features_popup()<cr>", desc = "Show features popup" },
    { "<leader>rcd", "<cmd>lua require('crates').show_dependencies_popup()<cr>", desc = "Show dependencies popup" },
    { "<leader>rcu", "<cmd>lua require('crates').update_crate()<cr>", desc = "Update crate" },
    { "<leader>rca", "<cmd>lua require('crates').update_all_crates()<cr>", desc = "Update all crates" },
    { "<leader>rcU", "<cmd>lua require('crates').upgrade_crate()<cr>", desc = "Upgrade crate" },
    { "<leader>rcA", "<cmd>lua require('crates').upgrade_all_crates()<cr>", desc = "Upgrade all crates" },
    {
      "<leader>rce",
      "<cmd>lua require('crates').expand_plain_crate_to_inline_table()<cr>",
      desc = "Expand plain crate to inline table",
    },
    { "<leader>rcE", "<cmd>lua require('crates').extract_crate_into_table()<cr>", desc = "Extract crate into table" },
    { "<leader>rcH", "<cmd>lua require('crates').open_homepage()<cr>", desc = "Open crate homepage" },
    { "<leader>rcR", "<cmd>lua require('crates').open_repository()<cr>", desc = "Open crate repository" },
    { "<leader>rcD", "<cmd>lua require('crates').open_documentation()<cr>", desc = "Open crate documentation" },
    { "<leader>rcC", "<cmd>lua require('crates').open_crates_io()<cr>", desc = "Open crates.io" },

    -- Search group
    { "<leader>s", group = "Search" },
    { "<leader>ss", "<cmd>lua require('searchbox').incsearch()<cr>", desc = "Incremental search" },
    { "<leader>sa", "<cmd>lua require('searchbox').match_all()<cr>", desc = "Match all" },
    { "<leader>sr", "<cmd>lua require('searchbox').replace()<cr>", desc = "Replace" },
    { "<leader>so", "<cmd>lua require('spectre').open()<cr>", desc = "Open search dialogue" },
    { "<leader>sw", "<cmd>lua require('spectre').open_visual({select_word=true})<cr>", desc = "Search current word" },
    { "<leader>sf", "<cmd>lua require('spectre').open_file_search()<cr>", desc = "Search in current file" },

    -- File Tree group
    { "<leader>t", group = "File Tree" },
    { "<leader>tt", "<cmd>Neotree toggle<cr>", desc = "Toggle", cond = not vim.g.vscode },
    { "<leader>tt", "<cmd>lua require('vscode').action('workbench.action.toggleSidebarVisibility')<cr>", desc = "Toggle", cond = vim.g.vscode ~= nil },

    -- Test group
    { "<leader>T", group = "Test" },
    { "<leader>Tr", "<cmd>lua require('neotest').run.run()<cr>", desc = "Run" },
    { "<leader>Td", "<cmd>lua require('neotest').run.run(){strategy = 'dap'}<cr>", desc = "Debug" },

    -- Undotree group
    { "<leader>u", group = "Undotree" },
    { "<leader>ut", "<cmd>UndotreeToggle<cr><cmd>UndotreeFocus<cr>", desc = "Toggle" },
    { "<leader>uf", "<cmd>UndotreeToggle<cr><cmd>UndotreeFocus<cr>", desc = "Focus" },

    -- Trouble group
    { "<leader>x", group = "Trouble" },
    { "<leader>xd", "<cmd>Trouble workspace_diagnostics<cr>", desc = "Diagnostics" },
    { "<leader>xq", "<cmd>Trouble quickfix<cr>", desc = "Quickfix" },

    { "<F5>", "<cmd>lua require'dap'.step_into()<cr>", desc = "Step into", cond = not vim.g.vscode },
    { "<F6>", "<cmd>lua require'dap'.step_over()<cr>", desc = "Step over", cond = not vim.g.vscode },
    { "<F7>", "<cmd>lua require'dap'.step_out()<cr>", desc = "Step out", cond = not vim.g.vscode },
    { "<F8>", "<cmd>lua require'dap'.continue()<cr>", desc = "Continue", cond = not vim.g.vscode },
  })
end

return M
