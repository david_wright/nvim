vim.g.mapleader = " "

package.path = package.path .. ";" .. "~/.luarocks/share/lua/5.1/?/init.lua;"
package.path = package.path .. ";" .. "~/.luarocks/share/lua/5.1/?.lua;"

-- plugins
require("plugins")
require("keybindings").setup()

-- vim settings
local set = vim.opt

set.number = true
set.relativenumber = true
set.nu = true
set.rnu = true
set.mouse = "a"

set.spell = true
vim.opt.spelllang = "en_gb"

set.ic = true

set.tabstop = 2
set.shiftwidth = 2
set.expandtab = true
set.smartindent = true
set.termguicolors = true

set.clipboard = "unnamedplus"

-- Disable logs, as the log file grows infinitely
vim.lsp.set_log_level("OFF")

local undodir = vim.fn.expand("~/.nvim/undodir")
if vim.fn.isdirectory(undodir) == 0 then
  vim.fn.mkdir(undodir, "", 0700)
end

set.undodir = undodir
set.undofile = true

-- Stop gitcommit from auto wrapping
vim.api.nvim_exec(
  [[
    augroup MyGitCommitAutocmds
        autocmd!
        autocmd FileType gitcommit,NeogitCommitMessage lua vim.opt_local.formatoptions:remove("tl")
    augroup END
]],
  false
)
