#! /usr/bin/env bash
# pip install compdb
# sudo npm i -g tree-sitter-cl-cli

#detecting the OS
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  sudo apt install -y \
    ripgrep \
    imagemagick \
    ffmpegthumbnailer \
    eog

  pushd /tmp/ &&
    git clone --depth 1 https://github.com/hpjansson/chafa &&
    pushd chafa &&
    ./autogen.sh &&
    make && sudo make install &&
    sudo ldconfig &&
    popd
  popd
elif [[ "$OSTYPE" == "darwin"* ]]; then
  brew install ripgrep \
  chafa \
  fzf \
  go \
  imagemagick \
  pkgconfig
fi
